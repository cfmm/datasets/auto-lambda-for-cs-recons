clear all; close all; clc;

load('knee_gt.mat','magnitude_mask')
load('knee.mat');
ksp = ksp/max(max(max(abs(ifft2c(ksp)))));
[sy,sz,sc] = size(ksp);

ksize = [6,6];

% the size of the kernel affects:
% 1. Sensitivity Coils Maps estimation
ax = 2; % sub-sampling factor in x
ay = 2; % sub-sampling factor in y

% biggest cl as reference
max_calibration_lines = 60;

C = Identity;
M = Identity;
P = Identity;

number_of_USPs = 4; 
% 1. Uniform Poisson
% 2. Variable-density Exponential-based
% 3. directional tensor based

ncalib = [10:10:60];
% the number of calibration lines affects:
% 1. Sensitivity Coils Maps estimation
% 2. Windowing to remove the arbitrary DC phase
% 3. Determination of the lambda parameter
% 4. Overall, the reconstruction performance

recons_Scomplete_total = zeros(numel(magnitude_mask),length(ncalib),number_of_USPs);
recons_total = zeros(numel(magnitude_mask),length(ncalib),number_of_USPs);
masks_total = zeros(numel(magnitude_mask),length(ncalib),number_of_USPs);
xsubs_total = zeros(numel(magnitude_mask),length(ncalib),number_of_USPs);

lambda1_total = zeros(length(ncalib),number_of_USPs);
lambda2_total = zeros(length(ncalib),number_of_USPs);
lambda3_total = zeros(length(ncalib),number_of_USPs);

nmse_total = zeros(length(ncalib),number_of_USPs);
nmse_Scomplete_total = zeros(length(ncalib),number_of_USPs);

nmse = @(a,b) norm(a(:) - b(:))^2 / norm(b(:))^2;

for kappa = 1 : number_of_USPs
    
    switch kappa
        case 1
            mask = vdPoisMex(sy, sz, sy, sz, ax, ay, max_calibration_lines, 0, 2);
        case 2
            mask = vdPoisMex(sy, sz, sy, sz, ax, ay, max_calibration_lines, 0, 0);
        case 3
            mask = wangs_VD_undersampling_pattern(size(magnitude_mask),ax*ay,3,4.5,'Calibration Lines',max_calibration_lines);
        case 4
            mask = wangs_VD_undersampling_pattern(size(magnitude_mask),1,3,4.5,'Calibration Lines',max_calibration_lines,...
                                                  'Parallel Imaging','2x2');
    end
    
    mask = repmat(mask,[1,1,sc]);
    y = mask .* ksp;
    [maps, weights] = ecalib(y, max_calibration_lines, ksize);
    Sx = ESPIRiT(maps, weights);

    cc = 1;
    for i = ncalib
        % under-sampled version
        switch kappa
            case 1
                mask = vdPoisMex(sy, sz, sy, sz, ax, ay, i, 0, 2);
            case 2
                mask = vdPoisMex(sy, sz, sy, sz, ax, ay, i, 0, 0);
            case 3
                mask = wangs_VD_undersampling_pattern(size(magnitude_mask),ax*ay,3,4.5,'Calibration Lines',i);            
            case 4
                mask = wangs_VD_undersampling_pattern(size(magnitude_mask),1,3,4.5,'Calibration Lines',i,...
                                                  'Parallel Imaging','2x2'); 
        end

        masks_total(:,cc,kappa) = mask(:);

        mask = repmat(mask,[1,1,sc]);
        
        y = mask .* ksp;
        F = p2DFT(mask,[sy, sz, sc]);
        if i ~= 10
            ksize = [6,6]; 
        else
            
            ksize = [4,4];
        end
        [maps, weights] = ecalib(y, i, ksize);
        S = ESPIRiT(maps, weights);

        x = Sx' * (ifft2c(ksp));

        x_sub = S' * ( F' * y );
        xsubs_total(:,cc,kappa) = x_sub(:);
        
        xsubs_Smax = Sx' * ( F' * y );

        wname = 'db4';
        tic
        [x_recon1, convergence_rate1, L1, Lambda1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                                'Lambda','multiple','Levels',3);
        toc
        disp(L1);
        recons_total(:,cc,kappa) = x_recon1(:);
        
        lambda1_total(cc,kappa) = Lambda1(4); % level 1
        lambda2_total(cc,kappa) = Lambda1(3); % level 2
        lambda3_total(cc,kappa) = Lambda1(2); % level 3

        mRec = abs(x_recon1).*magnitude_mask;
        mGT = abs(x) .* magnitude_mask;

        phiRec = angle(x_recon1).*magnitude_mask;
        phiGT = angle(x).*magnitude_mask;

        nmse_total(cc,kappa) = nmse(mRec,mGT);

        disp(strcat('number of calibration lines:',num2str(ncalib(cc)),' NMSE:',num2str(nmse(mRec,mGT))));

        
        tic
            [x_recon1, convergence_rate1, L1, Lambda1] = complex_mFISTA2(y,F,Sx,C,xsubs_Smax,wname,...
                                                'Lambda','multiple','Levels',3);
        toc

        recons_Scomplete_total(:,cc,kappa) = x_recon1(:);

        mRec = abs(x_recon1).*magnitude_mask;
        mGT = abs(x) .* magnitude_mask;

        phiRec = angle(x_recon1).*magnitude_mask;
        phiGT = angle(x).*magnitude_mask;

        nmse_Scomplete_total(cc,kappa) = nmse(mRec,mGT);

        cc = cc + 1;
    end
end

%
save('results_ups','masks_total','recons_total','magnitude_mask','nmse_total')
%%

%%

res = @(im) reshape(im,[320,320]);
figure, imshow(res(masks_total(:,2,1)) ,[]);
saveas(gcf,'new_fig5a','epsc');
figure, imshow(res(masks_total(:,2,2)),[]);
saveas(gcf,'new_fig5b','epsc');
figure, imshow(res(masks_total(:,2,3)) ,[]);
saveas(gcf,'new_fig5c','epsc');
figure, imshow(res(masks_total(:,2,4)),[]);
saveas(gcf,'new_fig5d','epsc');
%%
cl10 = 1;
figure, imshow(res(abs(recons_total(:,cl10,1))) .* magnitude_mask,[]);
saveas(gcf,'new_fig5e','epsc');
figure, imshow(res(abs(recons_total(:,cl10,2))).* magnitude_mask,[]);
saveas(gcf,'new_fig5f','epsc');
figure, imshow(res(abs(recons_total(:,cl10,3))) .* magnitude_mask,[]);
saveas(gcf,'new_fig5g','epsc');
figure, imshow(res(abs(recons_total(:,cl10,4))).* magnitude_mask,[]);
saveas(gcf,'new_fig5h','epsc');
%%
cl20 = 2;
figure, imshow(res(abs(recons_total(:,cl20,1))) .* magnitude_mask,[]);
saveas(gcf,'new_fig5i','epsc');
figure, imshow(res(abs(recons_total(:,cl20,2))).* magnitude_mask,[]);
saveas(gcf,'new_fig5j','epsc');
figure, imshow(res(abs(recons_total(:,cl20,3))) .* magnitude_mask,[]);
saveas(gcf,'new_fig5k','epsc');
figure, imshow(res(abs(recons_total(:,cl20,4))).* magnitude_mask,[]);
saveas(gcf,'new_fig5l','epsc');
%%
cl60 = 6;
figure, imshow(res(abs(recons_total(:,cl60,1))) .* magnitude_mask,[]);
saveas(gcf,'new_fig5m','epsc');
figure, imshow(res(abs(recons_total(:,cl60,2))).* magnitude_mask,[]);
saveas(gcf,'new_fig5n','epsc');
figure, imshow(res(abs(recons_total(:,cl60,3))) .* magnitude_mask,[]);
saveas(gcf,'new_fig5o','epsc');
figure, imshow(res(abs(recons_total(:,cl60,4))).* magnitude_mask,[]);
saveas(gcf,'new_fig5p','epsc');
%% supporting information S6

yellow_ = [0.9290, 0.6940, .1250];
green_ = [0.4660,0.6740,0.1880];
purple_ = [0.4940, 0.1840, 0.5560];
blue_ = [0, 0.4470, 0.7410];
colours = {yellow_,green_,blue_,purple_};

figure, 
b = bar(nmse_total);
b(1).FaceColor = yellow_;
b(2).FaceColor = green_;
b(3).FaceColor = blue_;
b(4).FaceColor = purple_;

legend('VP','UP','EP','PI');
%axis([0.5,6.5,0,0.025])
ylabel('NMSE','FontSize',28)
xlabel('Calibration Lines','FontSize',28)

xticks([1:6]);
xticklabels({'10','20','30','40','50','60'});
xtickangle(0);
set(gcf,'color','w');
set(gca,'LineWidth',2,'FontSize',20);

saveas(gcf,'figS6','epsc');