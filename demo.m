clear all; close all; clc;

% add scripts from this directory to the path
addpath(genpath('./'));

load('knee.mat');
ksp = ksp/max(max(max(abs(ifft2c(ksp)))));
[sy,sz,sc] = size(ksp);
ncalib = 20; ksize = [6,6];
ax = 2; % sub-sampling factor in x
ay = 2; % sub-sampling factor in y

mask = vdPoisMex(sy, sz, sy, sz, ax, ay, ncalib, 0, 2);
%mask = repmat(mask,[1,1,sz]);
mask = repmat(mask,[1,1,sc]);
%

y = mask .* ksp;
[maps, weights] = ecalib(y, ncalib, ksize);
%
% Create linear operators

C = Identity;

S = ESPIRiT(maps, weights);
%
F = p2DFT(mask,[sy, sz, sc]);

M = Identity;

P = Identity;

% Get Fully-sampled Image

x = S' * (ifft2c(ksp));


figure, imshowf(abs(x), [0, 1.0])
figure, imshowf(angle(x) .* (abs(x) > 0.05), [-pi, pi])


% Zero-filled recon

x_sub = S' * ( F' * y );
figure, imshowf(abs(x_sub), [0, 1.0])
figure, imshowf(abs(abs(x_sub) - abs(x)), [0, 0.1])
figure, imshowf(angle(x_sub) .* (abs(x) > 0.05), [-pi, pi])

magnitude_mask = or(abs(real(x_sub))>0.1, abs(imag(x_sub))>0.1);
figure, imshow(magnitude_mask)

% generate testing data
% knee = x;
% knee_mask = magnitude_mask;
% save('knee_2','knee','knee_mask');

%

% magnitude and phase images.
wname = 'dmey';
tic
[x_recon, convergence_rate, L, Lambda] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                        'Lambda','multiple','Levels','automatic');
toc

figure, imshow([abs(x_recon),abs(x)],[]);
