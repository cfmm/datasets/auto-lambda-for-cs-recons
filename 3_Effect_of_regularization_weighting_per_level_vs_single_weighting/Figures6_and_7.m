clear all; close all; clc;

load('knee.mat');
ksp = ksp/max(max(max(abs(ifft2c(ksp)))));
[sy,sz,sc] = size(ksp);
ncalib = 20; ksize = [6,6]; % 5-10 minutos en este formato
ax = 2; % sub-sampling factor in x
ay = 2; % sub-sampling factor in y

mask = vdPoisMex(sy, sz, sy, sz, ax, ay, ncalib, 0, 2);
%mask = repmat(mask,[1,1,sz]);
mask = repmat(mask,[1,1,sc]);
%

y = mask .* ksp;
[maps, weights] = ecalib(y, ncalib, ksize);
%
% Create linear operators

C = Identity;

S = ESPIRiT(maps, weights);
%
F = p2DFT(mask,[sy, sz, sc]);

M = Identity;

P = Identity;

% Get Fully-sampled Image

x = S' * (ifft2c(ksp));

figure, imshowf(abs(x), [0, 1.0])
figure, imshowf(angle(x) .* (abs(x) > 0.05), [-pi, pi])

%figure, imshow((abs(x) > 0.07))


% Zero-filled recon

x_sub = S' * ( F' * y );
figure, imshowf(abs(x_sub), [0, 1.0])
figure, imshowf(abs(abs(x_sub) - abs(x)), [0, 0.1])
figure, imshowf(angle(x_sub) .* (abs(x) > 0.05), [-pi, pi])

magnitude_mask = or(abs(real(x_sub))>0.1, abs(imag(x_sub))>0.1);
figure, imshow(magnitude_mask)

%norm = @(im) single((im - min(im(:))) * ((256-0)/(max(im(:)) - min(im(:)))) + 1);
%mag = imsegkmeans(norm(abs(x_sub)),2);

% magnitude and phase images.
%% different levels for single or multiple lambdas
minx = min(vec(abs(x)));
maxx = max(vec(abs(x)));
nmse = @(a,b) norm(a(:) - b(:))^2 / norm(b(:))^2;

mGT = abs(x) .* magnitude_mask;
phiGT = angle(x) .* magnitude_mask;
wname = 'db4';
% multiple
tic
[x_recon1, convergence_rate1, L1, Lambda1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                        'Lambda','multiple','Levels',3);
toc

mRec = abs(x_recon1).*magnitude_mask;
phiRec = angle(x_recon1).*magnitude_mask;
figure, imshow([mRec],[minx,maxx]);
saveas(gcf,'fig6a','epsc');
disp(num2str(nmse(mRec,mGT)));
save('recon_db4_level_3','x_recon1'); 

%%
tic
[x_recon1, convergence_rate1, L1, Lambda1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                        'Lambda','multiple','Levels','automatic');
toc
mRec = abs(x_recon1).*magnitude_mask;
phiRec = angle(x_recon1).*magnitude_mask;
figure, imshow([mRec],[minx,maxx]);
saveas(gcf,'fig6b','epsc');
disp(num2str(nmse(mRec,mGT)));
save('recon_db4_level_4','x_recon1'); 
%%
tic
[x_recon1, convergence_rate1, L1, Lambda1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                        'Lambda','multiple','Levels',5);
toc
mRec = abs(x_recon1).*magnitude_mask;
phiRec = angle(x_recon1).*magnitude_mask;
figure, imshow([mRec],[minx,maxx]);
saveas(gcf,'fig6c','epsc');
disp(num2str(nmse(mRec,mGT)));
save('recon_db4_level_5','x_recon1'); 

tic
[x_recon1, convergence_rate1, L1, Lambda1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                        'Lambda','multiple','Levels',6);
toc
mRec = abs(x_recon1).*magnitude_mask;
phiRec = angle(x_recon1).*magnitude_mask;
figure, imshow([mRec],[minx,maxx]);
saveas(gcf,'fig6d','epsc');
disp(num2str(nmse(mRec,mGT)));
save('recon_db4_level_6','x_recon1'); 

%%
% single
minx = min(vec(abs(x)));
maxx = max(vec(abs(x)));
nmse = @(a,b) norm(a(:) - b(:))^2 / norm(b(:))^2;

mGT = abs(x) .* magnitude_mask;
phiGT = angle(x) .* magnitude_mask;
wname = 'db4';

tic
[x_recon1, convergence_rate1, L1, Lambda1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                        'Lambda','single','Levels',3);
toc
mRec = abs(x_recon1).*magnitude_mask;
phiRec = angle(x_recon1).*magnitude_mask;
figure, imshow([mRec],[minx,maxx]);
saveas(gcf,'fig6e','epsc');
disp(num2str(nmse(mRec,mGT)));
save('recon_db4_single_level_3','x_recon1'); 

tic
[x_recon1, convergence_rate1, L1, Lambda1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                        'Lambda','single','Levels','automatic');
toc
mRec = abs(x_recon1).*magnitude_mask;
phiRec = angle(x_recon1).*magnitude_mask;
figure, imshow([mRec],[minx,maxx]);
saveas(gcf,'fig6f','epsc');
disp(num2str(nmse(mRec,mGT)));
save('recon_db4_single_level_4','x_recon1'); 

tic
[x_recon1, convergence_rate1, L1, Lambda1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                        'Lambda','single','Levels',5);
toc
mRec = abs(x_recon1).*magnitude_mask;
phiRec = angle(x_recon1).*magnitude_mask;
figure, imshow([mRec],[minx,maxx]);
saveas(gcf,'fig6g','epsc');
disp(num2str(nmse(mRec,mGT)));
save('recon_db4_single_level_5','x_recon1'); 

tic
[x_recon1, convergence_rate1, L1, Lambda1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                        'Lambda','single','Levels',6);
toc
mRec = abs(x_recon1).*magnitude_mask;
phiRec = angle(x_recon1).*magnitude_mask;
figure, imshow([mRec],[minx,maxx]);
saveas(gcf,'fig6h','epsc');
disp(num2str(nmse(mRec,mGT)));
save('recon_db4_single_level_6','x_recon1'); 

%%

m_imRec = @(im, mask_) abs(im) .* mask_;
phi_imRec = @(im, mask_) angle(im) .* mask_;

m_data_rec = @(im, mask_) abs(im(mask_));
phi_data_rec = @(im, mask_) angle(im(mask_));

nmse_i = 1; pc_i = 2; hfen_i = 3; ssim_i = 4; 
%
nmse_m = @(im, gt, mask_) norm( m_data_rec(im,mask_) - m_data_rec(gt,mask_) )^2 ...
                      / norm( m_data_rec(gt,mask_) )^2;
                  
nmse_phi = @(im, gt, mask_) norm( phi_data_rec(gt.*conj(im),mask_) )^2 ...
                      / norm( phi_data_rec(gt,mask_) )^2;


gt = x;
m_indices_mat = zeros(4,4);

auto3 = load('recon_db4_level_3');
auto4 = load('recon_db4_level_4');
alter3 = load('recon_db4_single_level_3');
alter4 = load('recon_db4_single_level_4');

a3 = 1;
a4 = 2;
al3 = 3;
al4 = 4;

rec = auto3.x_recon1;
m_indices_mat( nmse_i    , a3 ) = nmse_m(rec,gt,magnitude_mask);
m_indices_mat( pc_i      , a3 ) = 1-pcCoeff( m_data_rec(rec,magnitude_mask) , m_data_rec(gt,magnitude_mask ) );
m_indices_mat( hfen_i    , a3 ) = compute_hfen( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
m_indices_mat( ssim_i    , a3 ) = 1-compute_xsim( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
%
rec = auto4.x_recon1;
m_indices_mat( nmse_i    , a4 ) = nmse_m(rec,gt,magnitude_mask);
m_indices_mat( pc_i      , a4 ) = 1-pcCoeff( m_data_rec(rec,magnitude_mask) , m_data_rec(gt,magnitude_mask ) );
m_indices_mat( hfen_i    , a4 ) = compute_hfen( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
m_indices_mat( ssim_i    , a4 ) = 1-compute_xsim( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );

rec = alter3.x_recon1;
m_indices_mat( nmse_i    , al3 ) = nmse_m(rec,gt,magnitude_mask);
m_indices_mat( pc_i      , al3) = 1-pcCoeff( m_data_rec(rec,magnitude_mask) , m_data_rec(gt,magnitude_mask ) );
m_indices_mat( hfen_i    , al3 ) = compute_hfen( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
m_indices_mat( ssim_i    , al3 ) = 1-compute_xsim( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );

rec = alter4.x_recon1;
m_indices_mat( nmse_i    , al4 ) = nmse_m(rec,gt,magnitude_mask);
m_indices_mat( pc_i      , al4 ) = 1-pcCoeff( m_data_rec(rec,magnitude_mask) , m_data_rec(gt,magnitude_mask ) );
m_indices_mat( hfen_i    , al4 ) = compute_hfen( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
m_indices_mat( ssim_i    , al4 ) = 1-compute_xsim( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );

indices_labels = {'\textsf{NMSE \ [\%]} ','\textsf{1 \ - \ PC \ [\%]}','\textsf{HFEN \ [\%]} ','\textsf{1 \ - \ SSIM \ [\%]}'};
methods_labels = {'\mathsf{\lambda}_{\textsf{auto}} \textsf{, L=3 }','\mathsf{\lambda}_{\textsf{auto}} \textsf{, L=4 }','\mathsf{\lambda}_{\textsf{alter}} \textsf{, L=3 }','\mathsf{\lambda}_{\textsf{alter}} \textsf{, L=4 }'};
av_spider(100*m_indices_mat,indices_labels,methods_labels);
