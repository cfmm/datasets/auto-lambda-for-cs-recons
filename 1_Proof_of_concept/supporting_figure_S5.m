clear all; close all; clc;

load('knee.mat');
ksp = ksp/max(max(max(abs(ifft2c(ksp)))));
[sy,sz,sc] = size(ksp);
ncalib = 20; ksize = [6,6]; % 1 minute under this format
ax = 2; % sub-sampling factor in x
ay = 2; % sub-sampling factor in y

mask = vdPoisMex(sy, sz, sy, sz, ax, ay, ncalib, 0, 2);
mask = repmat(mask,[1,1,sc]);

y = mask .* ksp;
[maps, weights] = ecalib(y, ncalib, ksize);

% Create linear operators
C = Identity;
S = ESPIRiT(maps, weights);
F = p2DFT(mask,[sy, sz, sc]);
M = Identity;
P = Identity;

% Get Fully-sampled Image

x = S' * (ifft2c(ksp));

figure, imshowf(abs(x), [0, 1.0])
figure, imshowf(angle(x) .* (abs(x) > 0.05), [-pi, pi])

% Zero-filled recon

x_sub = S' * ( F' * y );
figure, imshowf(abs(x_sub), [0, 1.0])
figure, imshowf(abs(abs(x_sub) - abs(x)), [0, 0.1])
figure, imshowf(angle(x_sub) .* (abs(x) > 0.05), [-pi, pi])

% mask from magnitude -> definitely something to improve for future
% experiments
magnitude_mask = or(abs(real(x_sub))>0.1, abs(imag(x_sub))>0.1);
figure, imshow(magnitude_mask)

minx = min(vec(abs(x)));
maxx = max(vec(abs(x)));
nmse = @(a,b) norm(a(:) - b(:))^2 / norm(b(:))^2;

mGT = abs(x) .* magnitude_mask;
phiGT = angle(x) .* magnitude_mask;
%%
figure, imshow([mGT],[minx,maxx]);
saveas(gcf,'figs5d','epsc');
figure, imshow([phiGT],[-pi,pi]); 
saveas(gcf,'figs5h','epsc');
%%
% mother wavelet of choide
wname = 'dmey';

% optimization
tic
[x_recon1, convergence_rate1, L1, Lambda1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                        'Lambda','multiple','Levels','automatic');
toc
%
figure, imshow([abs(x_recon1).* magnitude_mask],[minx,maxx]);
saveas(gcf,'figs5a','epsc');
figure, imshow([angle(x_recon1).* magnitude_mask],[-pi,pi]); 
saveas(gcf,'figs5e','epsc');
%%
% mother wavelet of choide
wname = 'sym8';

% optimization
tic
[x_recon2, convergence_rate2, L2, Lambda2] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                        'Lambda','multiple','Levels','automatic');
toc

figure, imshow([abs(x_recon2).* magnitude_mask],[minx,maxx]);
saveas(gcf,'figs5b','epsc');
figure, imshow([angle(x_recon2).* magnitude_mask],[-pi,pi]); 
saveas(gcf,'figs5f','epsc');
%%
% mother wavelet of choide
wname = 'db4';

% optimization
tic
[x_recon3, convergence_rate3, L3, Lambda3] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                        'Lambda','multiple','Levels','automatic');
toc


figure, imshow([abs(x_recon3).* magnitude_mask],[minx,maxx]);
saveas(gcf,'figs5c','epsc');
figure, imshow([angle(x_recon3).* magnitude_mask],[-pi,pi]); 
saveas(gcf,'figs5g','epsc');
%%
m_data_rec = @(im, mask) abs(im(mask));
nmse_m = @(im, gt, mask) norm( m_data_rec(im,mask) - m_data_rec(gt,mask) )^2 ...
                      / norm( m_data_rec(gt,mask) )^2;
                  
phi_data_rec = @(im, mask) angle(im(mask));
nmse_phi = @(im, gt, mask) norm( phi_data_rec(gt.*conj(im),mask) )^2 ...
                      / norm( phi_data_rec(gt,mask) )^2;

nmse_total = zeros(2,3);
nmse_total(1,1) = nmse_m(x_recon1,x,magnitude_mask);
nmse_total(1,2) = nmse_m(x_recon2,x,magnitude_mask);
nmse_total(1,3) = nmse_m(x_recon3,x,magnitude_mask);
nmse_total(2,1) = nmse_phi(x_recon1, x, magnitude_mask);
nmse_total(2,2) = nmse_phi(x_recon2, x, magnitude_mask);
nmse_total(2,3) = nmse_phi(x_recon3, x, magnitude_mask);
figure , bar(nmse_total)

yellow_ = [0.9290, 0.6940, .1250];
green_ = [0.4660,0.6740,0.1880];
purple_ = [0.4940, 0.1840, 0.5560];
blue_ = [0, 0.4470, 0.7410];
colours = {yellow_,green_,blue_,purple_};

figure, 
b = bar(nmse_total);
b(1).FaceColor = yellow_;
b(2).FaceColor = green_;
b(3).FaceColor = blue_;

legend('dmeyer','symlet 8','daubechies 4');
axis([0.5,2.5,0,0.025])
ylabel('NMSE','FontSize',28)
%xlabel('$\textsf{\# Calibration Lines}$','Interpreter','Latex','FontSize',28)

xticks([1:2]);
xticklabels({'m','\phi'});
xtickangle(45);
set(gcf,'color','w');
set(gca,'LineWidth',2,'FontSize',24);

saveas(gcf,'figs5i','epsc');