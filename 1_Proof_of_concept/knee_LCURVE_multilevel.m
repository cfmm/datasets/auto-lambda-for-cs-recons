clear all; close all; clc;

load('knee.mat');
ksp = ksp/max(max(max(abs(ifft2c(ksp)))));
[sy,sz,sc] = size(ksp);
ncalib = 20; ksize = [6,6]; % 5-10 minutos en este formato
ax = 2; % sub-sampling factor in x
ay = 2; % sub-sampling factor in y

mask = vdPoisMex(sy, sz, sy, sz, ax, ay, ncalib, 0, 2);
%mask = repmat(mask,[1,1,sz]);
mask = repmat(mask,[1,1,sc]);
%

y = mask .* ksp;
[maps, weights] = ecalib(y, ncalib, ksize);
%
% Create linear operators

C = Identity;

S = ESPIRiT(maps, weights);
%
F = p2DFT(mask,[sy, sz, sc]);

M = Identity;

P = Identity;

% Get Fully-sampled Image

x = S' * (ifft2c(ksp));

figure, imshowf(abs(x), [0, 1.0])
figure, imshowf(angle(x) .* (abs(x) > 0.05), [-pi, pi])

%figure, imshow((abs(x) > 0.07))


% Zero-filled recon

x_sub = S' * ( F' * y );
figure, imshowf( abs(x_sub), [0, 1.0])
figure, imshowf( abs(abs(x_sub) - abs(x)), [0, 0.1])
figure, imshowf( angle(x_sub) .* (abs(x) > 0.05), [-pi, pi])

magnitude_mask = or(abs(real(x_sub))>0.1, abs(imag(x_sub))>0.1);
figure, imshow(magnitude_mask)

%norm = @(im) single((im - min(im(:))) * ((256-0)/(max(im(:)) - min(im(:)))) + 1);
%mag = imsegkmeans(norm(abs(x_sub)),2);

% magnitude and phase images.
lambda_range = [1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0];
factors = [1/8 , 1/5 , 1/2 , 1 , 2 , 5 , 8];

Lr = length(lambda_range);
Lf = length(factors);

wname = 'db4';

nmse = @(a,b) norm(a(:) - b(:))^2 / norm(b(:))^2 ; 

Levels_range = [3,4,5,6];
recon_all_levels = zeros( numel(x_sub),length(Levels_range) );
recon_all_lambdas = zeros(length(Levels_range),1);

lrlc = 1;
for lrl = Levels_range
    recons_range = zeros(numel(x_sub),Lr);
    L1_range = zeros(Lr,1);
    DC_range = zeros(Lr,1);

    %
    for i = 1 : Lr
        tic
        % Yn, Convergence_rate, Level , Lambda, Final_F, Final_G
        [x_recon, convergence_rate, L, Lambdas, cost_dc, cost_l1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                                'Lambda','multiple','Levels', lrl , ...
                                                'Fixed Lambda', lambda_range(i));
        toc
        recons_range(:,i) = x_recon(:);
        L1_range(i) = cost_l1;
        DC_range(i) = cost_dc;

    end
    %
    distance_range = sqrt(L1_range/max(L1_range).^2 + DC_range/max(DC_range).^2);
    disp(distance_range);
    [val, pos] = min(distance_range);

    recons_factors = zeros(numel(x_sub),Lf);
    L1_factors = zeros(Lf,1);
    DC_factors = zeros(Lf,1);

    for i = 1 : Lf
        tic
        % Yn, Convergence_rate, Level , Lambda, Final_F, Final_G
        [x_recon, convergence_rate, L, Lambdas, cost_dc, cost_l1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                                'Lambda','multiple','Levels', lrl , ...
                                                'Fixed Lambda', lambda_range(pos)*factors(i));
        toc
        recons_factors(:,i) = x_recon(:);
        L1_factors(i) = cost_l1;
        DC_factors(i) = cost_dc;
    end
    distance_factors = sqrt(L1_factors/max(L1_factors).^2 + DC_factors/max(DC_factors).^2);

    disp(distance_factors);
    [val, pos2] = min(distance_factors);
    recon_Lcurve = reshape(recons_factors(:,pos2),[size(magnitude_mask)]);
    save(strcat('results_knee_Lcurve_level',num2str(lrl)),'recon_Lcurve');
    %
    figure, imshow(abs(recon_Lcurve).*magnitude_mask,[]);
    figure, imshow(angle(recon_Lcurve).*magnitude_mask,[]);
    
    recon_all_levels(:,lrlc) = recons_factors(:,pos2);
    recon_all_lambdas(lrlc) = lambda_range(pos)*factors(pos2);
    lrlc = lrlc + 1;
end
save(strcat('results_knee_Lcurve_all_levels'),'recon_all_levels','recon_all_lambdas');

%%

load results_knee_Lcurve_all_levels
x_sub = zeros(320,320);
r1 = reshape(recon_all_levels(:,1),size(x_sub));
figure, imshow(abs(r1).*magnitude_mask,[]);
saveas(gcf,'figure5e','epsc');
r2 = reshape(recon_all_levels(:,2),size(x_sub));
figure, imshow(abs(r2).*magnitude_mask,[]);
saveas(gcf,'figure5f','epsc');
r3 = reshape(recon_all_levels(:,3),size(x_sub));
figure, imshow(abs(r3).*magnitude_mask,[]);
saveas(gcf,'figure5g','epsc');
r4 = reshape(recon_all_levels(:,4),size(x_sub));
figure, imshow(abs(r4).*magnitude_mask,[]);
saveas(gcf,'figure5h','epsc');