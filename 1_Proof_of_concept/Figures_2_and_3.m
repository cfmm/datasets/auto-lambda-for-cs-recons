clear all; close all; clc;

load('knee_gt');
load('results_knee_NMSE');
load('results_knee_LCURVE');
load('results_knee_db4');

%%
% Artvis needs to be included

num_methods = 3; % lcurve NMSE and proposal;
num_indices = 4; % nmse, 1 - PC, HFEN, 1 - SSIM; 
m_indices_mat = zeros(num_indices,num_methods);
phi_indices_mat = zeros(num_indices,num_methods);


m_imRec = @(im, mask) abs(im) .* mask;
phi_imRec = @(im, mask) angle(im) .* mask;

m_data_rec = @(im, mask) abs(im(mask));
phi_data_rec = @(im, mask) angle(im(mask));

nmse_i = 1; pc_i = 2; hfen_i = 3; ssim_i = 4;
NMSE = 3; LCURVE = 1; PROP = 2;  
%
nmse_m = @(im, gt, mask) norm( m_data_rec(im,mask) - m_data_rec(gt,mask) )^2 ...
                      / norm( m_data_rec(gt,mask) )^2;
                  
nmse_phi = @(im, gt, mask) norm( phi_data_rec(gt.*conj(im),mask) )^2 ...
                      / norm( phi_data_rec(gt,mask) )^2;
                  
%%
minx = min(vec(abs(gt).*magnitude_mask));
maxx = max(vec(abs(gt).*magnitude_mask));

figure, imshow(abs(recon_Lcurve).*magnitude_mask,   [minx,maxx]);
saveas(gcf,'fig2a','epsc');
figure, imshow(abs(recon_NMSE).*magnitude_mask,     [minx,maxx]);
saveas(gcf,'fig2b','epsc');
figure, imshow(abs(recon_db4).*magnitude_mask,      [minx,maxx]); 
saveas(gcf,'fig2c','epsc');
figure, imshow(abs(gt).*magnitude_mask,             [minx,maxx]);
saveas(gcf,'fig2d','epsc');
    %%              
%%% fill m matrix


rec = recon_Lcurve;
m_indices_mat( nmse_i    , LCURVE ) = nmse_m(rec,gt,magnitude_mask);
m_indices_mat( pc_i      , LCURVE ) = 1-pcCoeff( m_data_rec(rec,magnitude_mask) , m_data_rec(gt,magnitude_mask ) );
m_indices_mat( hfen_i    , LCURVE ) = compute_hfen( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
m_indices_mat( ssim_i    , LCURVE ) = 1-compute_xsim( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
%
rec = recon_NMSE;
m_indices_mat( nmse_i    , NMSE ) = nmse_m(rec,gt,magnitude_mask);
m_indices_mat( pc_i      , NMSE ) = 1-pcCoeff( m_data_rec(rec,magnitude_mask) , m_data_rec(gt,magnitude_mask ) );
m_indices_mat( hfen_i    , NMSE ) = compute_hfen( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
m_indices_mat( ssim_i    , NMSE ) = 1-compute_xsim( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );

rec = recon_db4;
m_indices_mat( nmse_i    , PROP ) = nmse_m(rec,gt,magnitude_mask);
m_indices_mat( pc_i      , PROP ) = 1-pcCoeff( m_data_rec(rec,magnitude_mask) , m_data_rec(gt,magnitude_mask ) );
m_indices_mat( hfen_i    , PROP ) = compute_hfen( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
m_indices_mat( ssim_i    , PROP ) = 1-compute_xsim( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );

%% 
indices_labels = {'\textsf{NMSE \ [\%]} ','\textsf{1 \ - \ PC \ [\%]}','\textsf{HFEN \ [\%]} ','\textsf{1 \ - \ SSIM \ [\%]}'};
methods_labels = {'\mathsf{\lambda}_{\textsf{Lcurve}}','\mathsf{\lambda}_{\textsf{auto}}','\mathsf{\lambda}_{\textsf{NMSE}}'};
av_spider(100*m_indices_mat,indices_labels,methods_labels);
%%
%%% check dimensions and then saveas
saveas(gcf,'fig2e','epsc');
%%

minx = min(vec(angle(gt).*magnitude_mask));
maxx = max(vec(angle(gt).*magnitude_mask));

figure, imshow(angle(recon_Lcurve).*magnitude_mask,   [minx,maxx]);
saveas(gcf,'fig3a','epsc');
figure, imshow(angle(recon_NMSE).*magnitude_mask,     [minx,maxx]);
saveas(gcf,'fig3b','epsc');
figure, imshow(angle(recon_db4).*magnitude_mask,      [minx,maxx]); 
saveas(gcf,'fig3c','epsc');
figure, imshow(angle(gt).*magnitude_mask,             [minx,maxx]);
saveas(gcf,'fig3d','epsc');


%%
% %%% fill phi matrix

rec = recon_Lcurve;
phi_indices_mat( nmse_i    , LCURVE ) = nmse_phi(rec,gt,magnitude_mask);
phi_indices_mat( pc_i      , LCURVE ) = 1-pcCoeff( phi_data_rec(rec,magnitude_mask) , phi_data_rec(gt,magnitude_mask ) );
phi_indices_mat( hfen_i    , LCURVE ) = compute_hfen( phi_imRec(rec,magnitude_mask) , phi_imRec(gt,magnitude_mask) );
phi_indices_mat( ssim_i    , LCURVE ) = 1-compute_xsim( phi_imRec(rec,magnitude_mask) , phi_imRec(gt,magnitude_mask) );
%
rec = recon_NMSE;
phi_indices_mat( nmse_i    , NMSE ) = nmse_phi(rec,gt,magnitude_mask);
phi_indices_mat( pc_i      , NMSE ) = 1-pcCoeff( phi_data_rec(rec,magnitude_mask) , phi_data_rec(gt,magnitude_mask ) );
phi_indices_mat( hfen_i    , NMSE ) = compute_hfen( phi_imRec(rec,magnitude_mask) , phi_imRec(gt,magnitude_mask) );
phi_indices_mat( ssim_i    , NMSE ) = 1-compute_xsim( phi_imRec(rec,magnitude_mask) , phi_imRec(gt,magnitude_mask) );

rec = recon_db4;
phi_indices_mat( nmse_i    , PROP ) = nmse_phi(rec,gt,magnitude_mask);
phi_indices_mat( pc_i      , PROP ) = 1-pcCoeff( phi_data_rec(rec,magnitude_mask) , phi_data_rec(gt,magnitude_mask ) );
phi_indices_mat( hfen_i    , PROP ) = compute_hfen( phi_imRec(rec,magnitude_mask) , phi_imRec(gt,magnitude_mask) );
phi_indices_mat( ssim_i    , PROP ) = 1-compute_xsim( phi_imRec(rec,magnitude_mask) , phi_imRec(gt,magnitude_mask) );
%
av_spider(100*phi_indices_mat,indices_labels,methods_labels);
%%
%%% check dimensions and then saveas
saveas(gcf,'fig3e','epsc');
%%