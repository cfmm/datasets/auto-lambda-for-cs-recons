
clear all; close all; clc;

auto3 = load('recon_db4_level_3'); a3 = 1;
auto4 = load('recon_db4_level_4'); a4 = 2;
auto5 = load('recon_db4_level_5'); a5 = 3;
auto6 = load('recon_db4_level_6'); a6 = 4;

lc3 = load('results_knee_Lcurve_level3'); l3 = 1;
lc4 = load('results_knee_Lcurve_level4'); l4 = 2;
lc5 = load('results_knee_Lcurve_level5'); l5 = 3;
lc6 = load('results_knee_Lcurve_level6'); l6 = 4;

gtstruct = load('ground_truth_knee');

%%

m_imRec = @(im, mask) abs(im) .* mask;
phi_imRec = @(im, mask) angle(im) .* mask;

m_data_rec = @(im, mask) abs(im(mask));
phi_data_rec = @(im, mask) angle(im(mask));

nmse_i = 1; pc_i = 2; hfen_i = 3; ssim_i = 4; 
%
nmse_m = @(im, gt, mask) norm( m_data_rec(im,mask) - m_data_rec(gt,mask) )^2 ...
                      / norm( m_data_rec(gt,mask) )^2;
                  
nmse_phi = @(im, gt, mask) norm( phi_data_rec(gt.*conj(im),mask) )^2 ...
                      / norm( phi_data_rec(gt,mask) )^2;

%% figure 4a-d



%% figure 4e-h
magnitude_mask = gtstruct.magnitude_mask;
load results_knee_Lcurve_all_levels
x_sub = zeros(320,320);
r1 = reshape(recon_all_levels(:,1),size(x_sub));
figure, imshow(abs(r1).*magnitude_mask,[]);
saveas(gcf,'figure4e','epsc');
r2 = reshape(recon_all_levels(:,2),size(x_sub));
figure, imshow(abs(r2).*magnitude_mask,[]);
saveas(gcf,'figure4f','epsc');
r3 = reshape(recon_all_levels(:,3),size(x_sub));
figure, imshow(abs(r3).*magnitude_mask,[]);
saveas(gcf,'figure4g','epsc');
r4 = reshape(recon_all_levels(:,4),size(x_sub));
figure, imshow(abs(r4).*magnitude_mask,[]);
saveas(gcf,'figure4h','epsc');

%% figure 4i

magnitude_mask = gtstruct.magnitude_mask;
gt = gtstruct.x;
m_indices_mat = zeros(4,4);

rec = auto3.x_recon1;
m_indices_mat( nmse_i    , a3 ) = nmse_m(rec,gt,magnitude_mask);
m_indices_mat( pc_i      , a3 ) = 1-pcCoeff( m_data_rec(rec,magnitude_mask) , m_data_rec(gt,magnitude_mask ) );
m_indices_mat( hfen_i    , a3 ) = compute_hfen( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
m_indices_mat( ssim_i    , a3 ) = 1-compute_xsim( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
%
rec = auto4.x_recon1;
m_indices_mat( nmse_i    , a4 ) = nmse_m(rec,gt,magnitude_mask);
m_indices_mat( pc_i      , a4 ) = 1-pcCoeff( m_data_rec(rec,magnitude_mask) , m_data_rec(gt,magnitude_mask ) );
m_indices_mat( hfen_i    , a4 ) = compute_hfen( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
m_indices_mat( ssim_i    , a4 ) = 1-compute_xsim( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );

rec = auto6.x_recon1;
m_indices_mat( nmse_i    , a5 ) = nmse_m(rec,gt,magnitude_mask);
m_indices_mat( pc_i      , a5 ) = 1-pcCoeff( m_data_rec(rec,magnitude_mask) , m_data_rec(gt,magnitude_mask ) );
m_indices_mat( hfen_i    , a5 ) = compute_hfen( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
m_indices_mat( ssim_i    , a5 ) = 1-compute_xsim( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );

rec = lc3.recon_Lcurve;
m_indices_mat( nmse_i    , l6 ) = nmse_m(rec,gt,magnitude_mask);
m_indices_mat( pc_i      , l6 ) = 1-pcCoeff( m_data_rec(rec,magnitude_mask) , m_data_rec(gt,magnitude_mask ) );
m_indices_mat( hfen_i    , l6 ) = compute_hfen( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );
m_indices_mat( ssim_i    , l6 ) = 1-compute_xsim( m_imRec(rec,magnitude_mask) , m_imRec(gt,magnitude_mask) );

indices_labels = {'\textsf{NMSE \ [\%]} ','\textsf{1 \ - \ PC \ [\%]}','\textsf{HFEN \ [\%]} ','\textsf{1 \ - \ SSIM \ [\%]}'};
methods_labels = {'\mathsf{\lambda}_{\textsf{auto}}^{{3}}','\mathsf{\lambda}_{\textsf{auto}}^{{4}}','\mathsf{\lambda}_{\textsf{auto}}^{{6}}','\mathsf{\lambda}_{\textsf{Lcurve}}'};
av_spider(100*m_indices_mat,indices_labels,methods_labels);
%%
% check margins and save
saveas(gcf,'fig4i','epsc');