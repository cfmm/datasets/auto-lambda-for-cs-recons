clear all; close all; clc;

load('knee.mat');
ksp = ksp/max(max(max(abs(ifft2c(ksp)))));
[sy,sz,sc] = size(ksp);
ncalib = 20; ksize = [6,6]; % 5-10 minutos en este formato
ax = 2; % sub-sampling factor in x
ay = 2; % sub-sampling factor in y

mask = vdPoisMex(sy, sz, sy, sz, ax, ay, ncalib, 0, 2);
%mask = repmat(mask,[1,1,sz]);
mask = repmat(mask,[1,1,sc]);
%

y = mask .* ksp;
[maps, weights] = ecalib(y, ncalib, ksize);
%
% Create linear operators

C = Identity;

S = ESPIRiT(maps, weights);
%
F = p2DFT(mask,[sy, sz, sc]);

M = Identity;

P = Identity;

% Get Fully-sampled Image

x = S' * (ifft2c(ksp));

figure, imshowf(abs(x), [0, 1.0])
figure, imshowf(angle(x) .* (abs(x) > 0.05), [-pi, pi])

%figure, imshow((abs(x) > 0.07))


% Zero-filled recon

x_sub = S' * ( F' * y );
figure, imshowf(abs(x_sub), [0, 1.0])
figure, imshowf(abs(abs(x_sub) - abs(x)), [0, 0.1])
figure, imshowf(angle(x_sub) .* (abs(x) > 0.05), [-pi, pi])

magnitude_mask = or(abs(real(x_sub))>0.1, abs(imag(x_sub))>0.1);
figure, imshow(magnitude_mask)

%norm = @(im) single((im - min(im(:))) * ((256-0)/(max(im(:)) - min(im(:)))) + 1);
%mag = imsegkmeans(norm(abs(x_sub)),2);

% magnitude and phase images.
lambda_range = [1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0];
factors = [1/8 , 1/5 , 1/2 , 1 , 2 , 5 , 8];

Lr = length(lambda_range);
Lf = length(factors);



wname = 'db4';

nmse = @(a,b) norm(a(:) - b(:))^2 / norm(b(:))^2 ; 

recons_range = zeros(numel(x_sub),Lr);
nmse_range = zeros(Lr,1);
for i = 1 : Lr
    tic
    [x_recon, convergence_rate, L, Lambdas, cost_dc, cost_l1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                            'Lambda','multiple','Levels', 3 , ...
                                            'Fixed Lambda', lambda_range(i));
    toc
    recons_range(:,i) = x_recon(:);
    nmse_range(i) = nmse(x_recon,x);
end

disp(nmse_range);
[val, pos] = min(nmse_range);

recons_factors = zeros(numel(x_sub),Lf);
nmse_factors = zeros(Lf,1);
for i = 1 : Lf
    tic
    [x_recon, convergence_rate, L, Lambdas, cost_dc, cost_l1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                            'Lambda','multiple','Levels', 3 , ...
                                            'Fixed Lambda', lambda_range(pos)*factors(i));
    toc
    recons_factors(:,i) = x_recon(:);
    nmse_factors(i) = nmse(x_recon,x);
end

disp(nmse_factors);
[val, pos2] = min(nmse_factors);
recon_NMSE = reshape(recons_factors(:,pos2),[size(magnitude_mask)]);
lambda_final = lambda_range(pos)*factors(pos2);
save('results_knee_NMSE','recon_NMSE','magnitude_mask','lambda_final');
%
figure, imshow(abs(recon_NMSE).*magnitude_mask,[]);
figure, imshow(angle(recon_NMSE).*magnitude_mask,[]);
