clear all; close all; 
clc;
load knee_2

m_knee = abs(knee);
phi_knee = angle(knee) .* knee_mask;
%
figure,
subplot 121, imshow(m_knee ,[])
subplot 122, imshow( phi_knee .* knee_mask,[])

%% Magnitude and Phase analysis

decomp_level = 3;
wname = 'db4';
[sx,sy] = size(m_knee);
[coeffs_m, book_m] = wavedec2(m_knee,decomp_level,wname);
[coeffs_phi, book_phi] = wavedec2(phi_knee,decomp_level,wname);

[coeffs_real, book_real] = wavedec2(real(knee),decomp_level,wname);
[coeffs_imag, book_imag] = wavedec2(imag(knee),decomp_level,wname);

[Wm,im_Wm] = Wx_visualizer(coeffs_m, book_m, wname);
[Wphi,im_Wphi] = Wx_visualizer(coeffs_phi, book_phi, wname);

%% Generates part of figure 1 (FS)

figure, imshow(m_knee,[])
saveas(gcf,'fig1a','epsc');
[avWm,avim_Wm] = av_Wx_visualizer(coeffs_m, book_m, wname);
saveas(gcf,'fig1c','epsc');
%
figure, imshow(avWm,[])
saveas(gcf,'fig1b','epsc');

%%%% you need to grab the zero-filled recon...

%% Generates supporting information figures 9 and 10

figure, imshow(real(knee),[])
saveas(gcf,'sif9_a','epsc');
[avWm,avim_Wm] = av_Wx_visualizer(coeffs_real, book_real, wname);
saveas(gcf,'sif9_c','epsc');
%
figure, imshow(avWm,[])
saveas(gcf,'sif9_b','epsc');

figure, imshow(imag(knee),[])
saveas(gcf,'sif9_d','epsc');
[avWm,avim_Wm] = av_Wx_visualizer(coeffs_imag, book_imag, wname);
saveas(gcf,'sif9_f','epsc');
%
figure, imshow(avWm,[])
saveas(gcf,'sif9_e','epsc');

figure, imshow(phi_knee.* knee_mask,[])
saveas(gcf,'sif10_a','epsc');
[avWm,avim_Wm] = av_Wx_visualizer(coeffs_phi, book_phi, wname);
saveas(gcf,'sif10_c','epsc');
%
figure, imshow(avWm,[])
saveas(gcf,'sif10_b','epsc');
