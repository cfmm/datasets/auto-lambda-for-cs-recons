clear all; close all; clc;

prop = load('results_auto_lambda');
lcurve = load('results_LCURVE');
nmse = load('results_NMSE');
%%% PLOTS!!!

%% colours
orange_ = [0.85,0.325,0.098];
yellow_ = [0.9290, 0.6940, .1250];
green_ = [0.4660,0.6740,0.1880];
purple_ = [0.4940, 0.1840, 0.5560];
blue_ = [0, 0.4470, 0.7410];
colours = {yellow_,green_,blue_,purple_};

noise_level = [5,10,20,30];
USF = [3,4,5,6,7,8,9,10];

%% FIGURE 8
%% plots NMSE vs USF
%                Column 1:USF 30 || Column 2: USF 10 
% Row 1 Re              (A)              (B)
% Row 2 Im              (C)              (D)
% Row 3 m               (E)              (F)
% Row 4 phase           (G)              (H)
%% Column 1
%% re
sig = 4; %
dimi = 1;
m_means = [squeeze(mean(prop.re_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.re_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.re_nmse_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.re_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.re_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.re_nmse_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
axis([2.5,14,0.00,.05]);
%text(10.5,0.045,'SNR= 30','FontSize',28,'Color','k');

xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');
text(10.5,0.012,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
text(10.5,0.016,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
text(10.5,0.008,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig8a','epsc');
%% im

m_means = [squeeze(mean(prop.im_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.im_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.im_nmse_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.im_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.im_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.im_nmse_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
axis([2.5,14,0.00,.05]);

%text(10.5,0.045,'SNR= 30','FontSize',28,'Color','k');

xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');

%text(10.5,0.012,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
%text(10.5,0.016,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
%text(10.5,0.008,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig8c','epsc');
%% m

m_means = [squeeze(mean(prop.m_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.m_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.m_nmse_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.m_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.m_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.m_nmse_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');

axis([2.5,14,0.00,.05]);
%text(10.5,0.045,'SNR= 30','FontSize',28,'Color','k');

xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');
%text(10.5,0.008,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
%text(10.5,0.012,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
%text(10.5,0.004,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig8e','epsc');

%%
%% phi
m_means = [squeeze(mean(prop.phi_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.phi_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.phi_nmse_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.phi_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.phi_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.phi_nmse_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
%axis([2.5,8,0.00,.01]);
axis([2.5,14,0.00,.05]);
%text(10.5,0.045,'$\textrm{SNR}= 30$','Interpreter','Latex','FontSize',28,'Color','k');


xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');
%text(10.5,0.018,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
%text(10.5,0.022,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
%text(10.5,0.014,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

%text(2.8,0.009,'$\textrm{SNR}= 5$','Interpreter','Latex','FontSize',28,'Color','k');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig8g','epsc');
%%
%% re
sig = 2; %
dimi = 1;
m_means = [squeeze(mean(prop.re_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.re_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.re_nmse_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.re_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.re_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.re_nmse_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
axis([2.5,14,0.00,.2]);
%text(10.5,0.18,'$\textrm{SNR}= 10$','Interpreter','Latex','FontSize',28,'Color','k');


xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');
text(10.5,0.115,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
text(10.5,0.14,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
text(10.5,0.02,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig8b','epsc');
%% im

m_means = [squeeze(mean(prop.im_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.im_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.im_nmse_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.im_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.im_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.im_nmse_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
%axis([2.5,14,0.00,.05]);
xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');

%text(10.5,0.115,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
%text(10.5,0.14,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
%text(10.5,0.02,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

axis([2.5,14,0.00,.2]);
%text(10.5,0.18,'$\textrm{SNR}= 10$','Interpreter','Latex','FontSize',28,'Color','k');


set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig8d','epsc');
%% m
m_means = [squeeze(mean(prop.m_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.m_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.m_nmse_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.m_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.m_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.m_nmse_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
%axis([2.5,8,0.00,.01]);
xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');
%text(10.5,0.05,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
%text(10.5,0.07,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
%text(10.5,0.02,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

axis([2.5,14,0.00,.2]);
%text(10.5,0.18,'$\textrm{SNR}= 10$','Interpreter','Latex','FontSize',28,'Color','k');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig8f','epsc');

%%
%% phi
m_means = [squeeze(mean(prop.phi_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.phi_nmse_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.phi_nmse_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.phi_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.phi_nmse_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.phi_nmse_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
%axis([2.5,8,0.00,.01]);
xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');

%text(10.5,0.05,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
%text(10.5,0.07,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
%text(10.5,0.03,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

axis([2.5,14,0.00,.2]);
%text(10.5,0.18,'$\textrm{SNR}= 10$','Interpreter','Latex','FontSize',28,'Color','k');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig8h','epsc');
%% FIGURE 9
%% plots SSIM vs USF
%                Column 1:USF 30 || Column 2: USF 10 
% Row 1 Re              (A)              (B)
% Row 2 Im              (C)              (D)
% Row 3 m               (E)              (F)
% Row 4 phase           (G)              (H)
%% PC
usf = 2; % usf = 2 --> 4 || usf = 8 --> 10
re_means = [mean(prop.re_nmse_mat(:,:,usf),1);
           mean(lcurve.re_nmse_mat(:,:,usf),1);
           mean(nmse.re_nmse_mat(:,:,usf),1)]';
re_stds = [std(prop.re_nmse_mat(:,:,usf),[],1);
          std(lcurve.re_nmse_mat(:,:,usf),[],1);
          std(nmse.re_nmse_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',re_means,re_stds,'no gt');


axis([3,42,-0.00,.4]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');

text(31,0.08,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
text(31,0.12,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
text(31,0.04,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

set(gca,'lineWidth',2,'FontSize',26);
%text(30,0.35,'$\textrm{USF} = 4$','Interpreter','Latex','FontSize',28,'Color','k');
saveas(gcf,'fig9a','epsc');
%% 

im_means = [mean(prop.im_nmse_mat(:,:,usf),1);
           mean(lcurve.im_nmse_mat(:,:,usf),1);
           mean(nmse.im_nmse_mat(:,:,usf),1)]';
im_stds = [std(prop.im_nmse_mat(:,:,usf),[],1);
          std(lcurve.im_nmse_mat(:,:,usf),[],1);
          std(nmse.im_nmse_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',im_means,im_stds,'no gt');



axis([3,42,-0.00,.4]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');

%text(31,0.08,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
%text(31,0.12,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
%text(31,0.04,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

%text(30,0.35,'$\textrm{USF} = 4$','Interpreter','Latex','FontSize',28,'Color','k');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig9c','epsc');
%%

m_means = [mean(prop.m_nmse_mat(:,:,usf),1);
           mean(lcurve.m_nmse_mat(:,:,usf),1);
           mean(nmse.m_nmse_mat(:,:,usf),1)]';
m_stds = [std(prop.m_nmse_mat(:,:,usf),[],1);
          std(lcurve.m_nmse_mat(:,:,usf),[],1);
          std(nmse.m_nmse_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',m_means,m_stds,'no gt');


axis([3,42,-0.00,.4]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig9e','epsc');
%% m NMSE

m_means = [mean(prop.phi_nmse_mat(:,:,usf),1);
           mean(lcurve.phi_nmse_mat(:,:,usf),1);
           mean(nmse.phi_nmse_mat(:,:,usf),1)]';
m_stds = [std(prop.phi_nmse_mat(:,:,usf),[],1);
          std(lcurve.phi_nmse_mat(:,:,usf),[],1);
          std(nmse.phi_nmse_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',m_means,m_stds,'no gt');

set(gca,'lineWidth',2,'FontSize',22);
axis([3,42,-0.00,.4]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig9g','epsc');
%%

usf = 8; % usf = 2 --> 4 || usf = 8 --> 10
re_means = [mean(prop.re_nmse_mat(:,:,usf),1);
           mean(lcurve.re_nmse_mat(:,:,usf),1);
           mean(nmse.re_nmse_mat(:,:,usf),1)]';
re_stds = [std(prop.re_nmse_mat(:,:,usf),[],1);
          std(lcurve.re_nmse_mat(:,:,usf),[],1);
          std(nmse.re_nmse_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',re_means,re_stds,'no gt');

set(gca,'lineWidth',2,'FontSize',22);
axis([3,42,-0.00,.8]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');

text(31,0.2,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
text(31,0.3,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
text(31,0.1,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig9b','epsc');
%% 

im_means = [mean(prop.im_nmse_mat(:,:,usf),1);
           mean(lcurve.im_nmse_mat(:,:,usf),1);
           mean(nmse.im_nmse_mat(:,:,usf),1)]';
im_stds = [std(prop.im_nmse_mat(:,:,usf),[],1);
          std(lcurve.im_nmse_mat(:,:,usf),[],1);
          std(nmse.im_nmse_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',im_means,im_stds,'no gt');



axis([3,42,-0.00,.8]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');
set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig9d','epsc');
%%

m_means = [mean(prop.m_nmse_mat(:,:,usf),1);
           mean(lcurve.m_nmse_mat(:,:,usf),1);
           mean(nmse.m_nmse_mat(:,:,usf),1)]';
m_stds = [std(prop.m_nmse_mat(:,:,usf),[],1);
          std(lcurve.m_nmse_mat(:,:,usf),[],1);
          std(nmse.m_nmse_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',m_means,m_stds,'no gt');

set(gca,'lineWidth',2,'FontSize',22);
axis([3,42,-0.00,.8]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');
set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig9f','epsc');
%% m NMSE

m_means = [mean(prop.phi_nmse_mat(:,:,usf),1);
           mean(lcurve.phi_nmse_mat(:,:,usf),1);
           mean(nmse.phi_nmse_mat(:,:,usf),1)]';
m_stds = [std(prop.phi_nmse_mat(:,:,usf),[],1);
          std(lcurve.phi_nmse_mat(:,:,usf),[],1);
          std(nmse.phi_nmse_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',m_means,m_stds,'no gt');

set(gca,'lineWidth',2,'FontSize',22);
axis([3,42,-0.00,.8]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{NMSE}','Interpreter','latex');
set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'fig9h','epsc');

%% FIGURE 10 Images
load('mask_brain');
load('auto_db4_recon_noisy_phantom_noiselevel_30USF:4_experiment7.mat','x_recon','gt');

rot = @(im) imrotate(im,-90);

%
loc = @(im) (im(20:175,23:195));
figure, imshow(loc(rot(abs(x_recon).*mask)),[]);
saveas(gcf,'fig10g.eps','epsc');
figure, imshow(loc(rot(angle(x_recon).*mask)),[]);
saveas(gcf,'fig10j.eps','epsc');
figure, imshow(loc(rot(real(x_recon).*mask)),[]);
saveas(gcf,'fig10a.eps','epsc');
figure, imshow(loc(rot(imag(x_recon).*mask)),[]);
saveas(gcf,'fig10d.eps','epsc');
%%
figure, imshow(loc(rot(abs(gt).*mask)),[]);
saveas(gcf,'fig10h.eps','epsc');
figure, imshow(loc(rot(angle(gt).*mask)),[]);
saveas(gcf,'fig10k.eps','epsc');
figure, imshow(loc(rot(real(gt).*mask)),[]);
saveas(gcf,'fig10b.eps','epsc');
figure, imshow(loc(rot(imag(gt).*mask)),[]);
saveas(gcf,'fig10e.eps','epsc');

%%

load('auto_db4_recon_noisy_phantom_noiselevel_30USF:10_experiment3.mat','x_recon');
figure, imshow(loc(rot(abs(x_recon).*mask)),[]);
saveas(gcf,'fig10i.eps','epsc');
figure, imshow(loc(rot(angle(x_recon).*mask)),[]);
saveas(gcf,'fig10l.eps','epsc');
figure, imshow(loc(rot(real(x_recon).*mask)),[]);
saveas(gcf,'fig10c.eps','epsc');
figure, imshow(loc(rot(imag(x_recon).*mask)),[]);
saveas(gcf,'fig10f.eps','epsc');

%% SUPPORTING INFORMATION 6
%% plots {PC,HFEN,SSIM} vs USF
%                Column 1:USF 30 || Column 2: USF 10 
% Row 1 Re              (A)              (B)
% Row 2 Im              (C)              (D)
% Row 3 m               (E)              (F)
% Row 4 phase           (G)              (H)
%% Column 1
%% re
sig = 4; %
dimi = 1;
m_means = [squeeze(mean(prop.re_pc_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.re_pc_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.re_pc_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.re_pc_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.re_pc_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.re_pc_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
axis([2.5,14,0.00,.01]);

xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{1 - PC}','Interpreter','latex');

text(10.5,0.006,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
text(10.5,0.007,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
text(10.5,0.004,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp6a','epsc');
%% im

m_means = [squeeze(mean(prop.im_pc_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.im_pc_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.im_pc_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.im_pc_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.im_pc_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.im_pc_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
axis([2.5,14,0.00,.01]);

xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{1 - PC}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp6d','epsc');
%% m

m_means = [squeeze(mean(prop.m_pc_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.m_pc_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.m_pc_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.m_pc_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.m_pc_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.m_pc_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');

axis([2.5,14,0.00,.1]);
xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{1 - PC}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp6g','epsc');

%%
%% phi
m_means = [squeeze(mean(prop.phi_pc_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.phi_pc_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.phi_pc_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.phi_pc_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.phi_pc_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.phi_pc_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
%axis([2.5,8,0.00,.01]);
axis([2.5,14,0.00,.1]);
xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{1 - PC}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp6j','epsc');
%%
%% re

m_means = [squeeze(mean(prop.re_hfen_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.re_hfen_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.re_hfen_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.re_hfen_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.re_hfen_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.re_hfen_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
axis([2.5,14,0.00,.005]);


xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{HFEN}','Interpreter','latex');
text(10.5,0.0015,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
text(10.5,0.002,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
text(10.5,0.001,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp6b','epsc');
%% im

m_means = [squeeze(mean(prop.im_hfen_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.im_hfen_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.im_hfen_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.im_hfen_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.im_hfen_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.im_hfen_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
axis([2.5,14,0.00,.005]);

xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{HFEN}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp6e','epsc');
%% m
m_means = [squeeze(mean(prop.m_hfen_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.m_hfen_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.m_hfen_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.m_hfen_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.m_hfen_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.m_hfen_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
%axis([2.5,8,0.00,.01]);

axis([2.5,14,0.00,.005]);

xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{HFEN}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp6h','epsc');

%%
%% phi
m_means = [squeeze(mean(prop.phi_hfen_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.phi_hfen_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.phi_hfen_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.phi_hfen_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.phi_hfen_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.phi_hfen_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
%axis([2.5,8,0.00,.01]);

axis([2.5,14,0.00,.05]);
xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{HFEN}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp6k','epsc');
%%
m_means = [squeeze(mean(prop.re_ssim_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.re_ssim_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.re_ssim_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.re_ssim_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.re_ssim_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.re_ssim_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%

xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{1 - SSIM}','Interpreter','latex');
text(10.5,2e-5,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
text(10.5,3e-5,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
text(10.5,1e-5,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

axis([2.5,14,0.00,.5e-4]);

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp6c','epsc');
%% im

m_means = [squeeze(mean(prop.im_ssim_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.im_ssim_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.im_ssim_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.im_ssim_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.im_ssim_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.im_ssim_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
axis([2.5,14,0.00,.5e-4]);
xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{1 - SSIM}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp6f','epsc');
%% m

m_means = [squeeze(mean(prop.m_ssim_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.m_ssim_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.m_ssim_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.m_ssim_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.m_ssim_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.m_ssim_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');

axis([2.5,14,0.00,.1]);
text(10.5,0.095,'$\textrm{SNR}= 30$','Interpreter','Latex','FontSize',28,'Color','k');

%
%axis([2.5,8,0.00,.01]);

axis([2.5,14,0.00,.5e-4]);
xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{1 - SSIM}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp6i','epsc');

%%
%% phi
m_means = [squeeze(mean(prop.phi_ssim_mat(:,sig,:),dimi))';
           squeeze(mean(lcurve.phi_ssim_mat(:,sig,:),dimi))';
           squeeze(mean(nmse.phi_ssim_mat(:,sig,:),dimi))']';
       
m_stds = [squeeze(std(prop.phi_ssim_mat(:,sig,:),[],dimi))';
          squeeze(std(lcurve.phi_ssim_mat(:,sig,:),[],dimi))';
          squeeze(std(nmse.phi_ssim_mat(:,sig,:),[],dimi))']';
      
figure, av_plot(USF',m_means,m_stds,'no gt');
%
%axis([2.5,8,0.00,.01]);
axis([2.5,14,0.00,.3]);
xlabel('\textsf{USF [x]}','Interpreter','latex');
ylabel('\textsf{1 - SSIM}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp6l','epsc');
%%
%% SUPPORTING INFORMATION 7
%% plots SSIM vs USF
%                Column 1:USF 30 || Column 2: USF 10 
% Row 1 Re              (A)              (B)
% Row 2 Im              (C)              (D)
% Row 3 m               (E)              (F)
% Row 4 phase           (G)              (H)
%% PC
usf = 2; % usf = 2 --> 4 || usf = 8 --> 10

re_means = [mean(prop.re_pc_mat(:,:,usf),1);
           mean(lcurve.re_pc_mat(:,:,usf),1);
           mean(nmse.re_pc_mat(:,:,usf),1)]';
re_stds = [std(prop.re_pc_mat(:,:,usf),[],1);
          std(lcurve.re_pc_mat(:,:,usf),[],1);
          std(nmse.re_pc_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',re_means,re_stds,'no gt');


axis([3,42,0,.15]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{1 - PC}','Interpreter','latex');

text(31,0.025,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
text(31,0.04,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
text(31,0.01,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

set(gca,'lineWidth',2,'FontSize',26);
%text(30,.14,'$\textrm{USF} = 4$','Interpreter','Latex','FontSize',28,'Color','k');
saveas(gcf,'new_sp7a','epsc');
%% 

im_means = [mean(prop.im_pc_mat(:,:,usf),1);
           mean(lcurve.im_pc_mat(:,:,usf),1);
           mean(nmse.im_pc_mat(:,:,usf),1)]';
im_stds = [std(prop.im_pc_mat(:,:,usf),[],1);
          std(lcurve.im_pc_mat(:,:,usf),[],1);
          std(nmse.im_pc_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',im_means,im_stds,'no gt');



axis([3,42,0,.15]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{1 - PC}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
%text(30,.14,'$\textrm{USF} = 4$','Interpreter','Latex','FontSize',28,'Color','k');
saveas(gcf,'new_sp7d','epsc');
%%

m_means = [mean(prop.m_pc_mat(:,:,usf),1);
           mean(lcurve.m_pc_mat(:,:,usf),1);
           mean(nmse.m_pc_mat(:,:,usf),1)]';
m_stds = [std(prop.m_pc_mat(:,:,usf),[],1);
          std(lcurve.m_pc_mat(:,:,usf),[],1);
          std(nmse.m_pc_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',m_means,m_stds,'no gt');


axis([3,42,0,.9]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{1 - PC}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp7g','epsc');
%% m NMSE

m_means = [mean(prop.phi_pc_mat(:,:,usf),1);
           mean(lcurve.phi_pc_mat(:,:,usf),1);
           mean(nmse.phi_pc_mat(:,:,usf),1)]';
m_stds = [std(prop.phi_pc_mat(:,:,usf),[],1);
          std(lcurve.phi_pc_mat(:,:,usf),[],1);
          std(nmse.phi_pc_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',m_means,m_stds,'no gt');

axis([3,42,0,.45]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{1 - PC}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp7j','epsc');

%%

re_means = [mean(prop.re_hfen_mat(:,:,usf),1);
           mean(lcurve.re_hfen_mat(:,:,usf),1);
           mean(nmse.re_hfen_mat(:,:,usf),1)]';
re_stds = [std(prop.re_hfen_mat(:,:,usf),[],1);
          std(lcurve.re_hfen_mat(:,:,usf),[],1);
          std(nmse.re_hfen_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',re_means,re_stds,'no gt');


axis([3,42,0,20e-3]);

text(31,4e-3,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
text(31,6e-3,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
text(31,2e-3,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});

xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{HFEN}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp7b','epsc');
%% 

im_means = [mean(prop.im_hfen_mat(:,:,usf),1);
           mean(lcurve.im_hfen_mat(:,:,usf),1);
           mean(nmse.im_hfen_mat(:,:,usf),1)]';
im_stds = [std(prop.im_hfen_mat(:,:,usf),[],1);
          std(lcurve.im_hfen_mat(:,:,usf),[],1);
          std(nmse.im_hfen_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',im_means,im_stds,'no gt');

axis([3,42,0,20e-3]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{HFEN}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp7e','epsc');
%%

m_means = [mean(prop.m_hfen_mat(:,:,usf),1);
           mean(lcurve.m_hfen_mat(:,:,usf),1);
           mean(nmse.m_hfen_mat(:,:,usf),1)]';
m_stds = [std(prop.m_hfen_mat(:,:,usf),[],1);
          std(lcurve.m_hfen_mat(:,:,usf),[],1);
          std(nmse.m_hfen_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',m_means,m_stds,'no gt');


axis([3,42,0,30e-3]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{HFEN}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp7h','epsc');
%% m NMSE

m_means = [mean(prop.phi_hfen_mat(:,:,usf),1);
           mean(lcurve.phi_hfen_mat(:,:,usf),1);
           mean(nmse.phi_hfen_mat(:,:,usf),1)]';
m_stds = [std(prop.phi_hfen_mat(:,:,usf),[],1);
          std(lcurve.phi_hfen_mat(:,:,usf),[],1);
          std(nmse.phi_hfen_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',m_means,m_stds,'no gt');

axis([3,42,0,.5]);

xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{HFEN}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp7k','epsc');
%%

usf = 2; % usf = 2 --> 4 || usf = 8 --> 10
re_means = [mean(prop.re_ssim_mat(:,:,usf),1);
           mean(lcurve.re_ssim_mat(:,:,usf),1);
           mean(nmse.re_ssim_mat(:,:,usf),1)]';
re_stds = [std(prop.re_ssim_mat(:,:,usf),[],1);
          std(lcurve.re_ssim_mat(:,:,usf),[],1);
          std(nmse.re_ssim_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',re_means,re_stds,'no gt');


axis([3,42,-1e-4,2e-3]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{1 - SSIM}','Interpreter','latex');

text(31,3e-4,'$\lambda_{\textrm{auto}}$','Interpreter','Latex','FontSize',28,'Color',colours{1});
text(31,5e-4,'$\lambda_{\textrm{Lcurve}}$','Interpreter','Latex','FontSize',28,'Color',colours{2});
text(31,1e-4,'$\lambda_{\textrm{NMSE}}$','Interpreter','Latex','FontSize',28,'Color',colours{3});


set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp7c','epsc');
%% 

im_means = [mean(prop.im_ssim_mat(:,:,usf),1);
           mean(lcurve.im_ssim_mat(:,:,usf),1);
           mean(nmse.im_ssim_mat(:,:,usf),1)]';
im_stds = [std(prop.im_ssim_mat(:,:,usf),[],1);
          std(lcurve.im_ssim_mat(:,:,usf),[],1);
          std(nmse.im_ssim_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',im_means,im_stds,'no gt');

axis([3,42,-1e-4,2e-3]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{1 - SSIM}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp7f','epsc');
%%

m_means = [mean(prop.m_ssim_mat(:,:,usf),1);
           mean(lcurve.m_ssim_mat(:,:,usf),1);
           mean(nmse.m_ssim_mat(:,:,usf),1)]';
m_stds = [std(prop.m_ssim_mat(:,:,usf),[],1);
          std(lcurve.m_ssim_mat(:,:,usf),[],1);
          std(nmse.m_ssim_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',m_means,m_stds,'no gt');


axis([3,42,-1e-4,2e-3]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{1 - SSIM}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp7i','epsc');
%% m NMSE

m_means = [mean(prop.phi_ssim_mat(:,:,usf),1);
           mean(lcurve.phi_ssim_mat(:,:,usf),1);
           mean(nmse.phi_ssim_mat(:,:,usf),1)]';
m_stds = [std(prop.phi_ssim_mat(:,:,usf),[],1);
          std(lcurve.phi_ssim_mat(:,:,usf),[],1);
          std(nmse.phi_ssim_mat(:,:,usf),[],1)];
figure, av_plot(noise_level',m_means,m_stds,'no gt');

axis([3,42,-0.00,1]);
xlabel('\textsf{SNR}','Interpreter','latex');
ylabel('\textsf{1 - SSIM}','Interpreter','latex');

set(gca,'lineWidth',2,'FontSize',26);
saveas(gcf,'new_sp7l','epsc');