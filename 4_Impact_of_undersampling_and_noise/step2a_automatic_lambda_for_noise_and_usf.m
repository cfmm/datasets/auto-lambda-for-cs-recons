%% 2020-03-24
% Reconstructions of noisy_phase_phantom
clear all; close all; clc;

SNR = [30,20,10,5];
number_experiment = [1:10];
USF = [3:10];
wname = 'db4';

% ESPIRIT VARIABLES
ncalib = 20; ksize = [6,6]; % 5-10 minutos en este formato

cd Simulations
mkdir('results_auto_lambda');

for i = SNR
    for k = USF
        for j = number_experiment

            disp(strcat('working for recon with SNR:',num2str(i),...
                        'with usf:',num2str(k),...               
                        'at experiment:',num2str(j)));

            load(strcat('noisy_phantom_SNR_',num2str(i),...
                                       '_experiment',num2str(j),'.mat'));
            %
            ksp = noisy_ksp;
            [sx,sy,sc] = size(ksp);
            
            %

            %%%%%%%%%
            % acceleration settings
            cs_acc = k;
            pi_acc = 1;

            ax = sqrt(cs_acc); % sub-sampling factor in x
            ay = sqrt(cs_acc); % sub-sampling factor in y

            %%%%%%%%%
            % CS contribution
            if cs_acc ~= 1
                mask_CS = vdPoisMex(sx, sy, sx, sy, ax, ay, ncalib, 0, 1.3);
            else
                mask_CS = ones(size(sx,sy));
            end

            % PI contribution % do Nothing .... for now
            mask_PI = ones(size(mask_CS)); 
            mask = mask_PI .* mask_CS;

            sum(numel(mask)/sum(mask(:)))
            mask = repmat(mask,[1,1,sc]);

            y = mask .* ksp;

            disp('Calculating Sensitivity Coil Maps');
            [maps, weights] = nDecalib(y, ncalib, ksize);
            %
            % Create linear operators
            disp('Creating Linear Operators');

            C = Identity;

            S = ESPIRiT(maps, weights);

            F = p2DFT(mask,[sx,sy, sc]);

            M = Identity;

            P = Identity;

            Ffull = p2DFT(ones(size(ksp)),[ sx, sy, sc ]);
            
            x = C* (S' * (Ffull'*ksp));

            minx = min(vec(abs(x)));
            maxx = max(vec(abs(x)));

            % Zero-filled k-space recon
            x_sub = S' * ( F'*( ksp ));
            
            disp('Rconstruction from adaptive L1 proximal operator');
            % function [x] = complex_mprecon(y, F, S, C, Pc, x, niter, dohogwild)
            
            tic
            [x_recon, convergence_rate, L, Lambdas, cost_dc, cost_l1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                                    'Lambda','multiple','Levels','automatic','Fixed Lambda',[]);
            toc

            nmse = @(a,b) norm(a(:) - b(:))^2 / norm(b(:))^2;
    
            gt = C* (S' * (Ffull'*ground_truth));
            nmse(real(x_recon).*mask2D,real(gt).*mask2D)
            nmse(imag(x_recon).*mask2D,imag(gt).*mask2D)
            nmse(abs(x_recon).*mask2D,abs(gt).*mask2D)

            % Please create the folder :)
            cd ./results_auto_lambda
                save(strcat('auto_',wname,'_recon_noisy_phantom_noiselevel_',num2str(i),...
                                            'USF:',num2str(k),...
                                           '_experiment',num2str(j),'.mat'),'x_recon','gt','x','weights','convergence_rate','number_experiment',...
                                                                            'L','Lambdas');

            cd ..

        end
    end
end
%%%
cd ..
cd Impact_of_undersampling_and_noise