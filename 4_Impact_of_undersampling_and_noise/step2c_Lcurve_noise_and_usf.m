%% 2020-03-24
% Reconstructions of noisy_phase_phantom
% just with 40% of the inner region of the histogram


clear all; close all; clc;

SNR = [30,20,10,5];
number_experiment = 1:10;
USF = [3:10];
wname = 'db4';

% ESPIRIT VARIABLES
ncalib = 20; ksize = [6,6]; % 5-10 minutos en este formato
cd Simulations  
mkdir('results_Lcurve')

for i = SNR
    for k = USF
        for j = number_experiment

            disp(strcat('working for recon with SNR:',num2str(i),...
                        'with usf:',num2str(k),...               
                        'at experiment:',num2str(j)));

            load(strcat('noisy_phantom_SNR_',num2str(i),...
                                       '_experiment',num2str(j),'.mat'));
            %
            disp(strcat('working for recon with SNR:',num2str(i),...
                        'with usf:',num2str(k),...               
                        'at experiment:',num2str(j)));

            load(strcat('noisy_phantom_SNR_',num2str(i),...
                                       '_experiment',num2str(j),'.mat'));
            %
            ksp = noisy_ksp;
            [sx,sy,sc] = size(ksp);

            %

            %%%%%%%%%
            % acceleration settings
            cs_acc = k;
            pi_acc = 1;

            ax = sqrt(cs_acc); % sub-sampling factor in x
            ay = sqrt(cs_acc); % sub-sampling factor in y

            %%%%%%%%%
            % CS contribution
            if cs_acc ~= 1
                mask_CS = vdPoisMex(sx, sy, sx, sy, ax, ay, ncalib, 0, 1.3);
            else
                mask_CS = ones(size(sx,sy));
            end

            % PI contribution
            mask_PI = ones(size(mask_CS)); %%% For next paper! 

            mask = mask_PI .* mask_CS;

            sum(numel(mask)/sum(mask(:)))
            %mask = repmat(mask,[1,1,sz]);
            %mask = permute(mask,[3,1,2]);
            mask = repmat(mask,[1,1,sc]);

            y = mask .* ksp;

            disp('Calculating Sensitivity Coil Maps');
            [maps, weights] = nDecalib(y, ncalib, ksize);
            %
            % Create linear operators
            disp('Creating Linear Operators');

            C = Identity;

            S = ESPIRiT(maps, weights);

            F = p2DFT(mask,[sx,sy, sc]);

            M = Identity;

            P = Identity;

            Ffull = p2DFT(ones(size(ksp)),[ sx, sy, sc ]);
            
            x = C* (S' * (Ffull'*ksp));

            minx = min(vec(abs(x)));
            maxx = max(vec(abs(x)));

            magnitude_mask_gt = double(abs(x)) >.1*maxx;

            % Zero-filled recon
            x_sub = S' * ( F'*( ksp ));
            magnitude_mask_x0 = double(abs(x_sub)) >.15*max(abs(x_sub(:)));  % Too simplistic the definition of the ROI 

            % magnitude and phase images.


            %
            disp('Rconstruction from adaptive L1 proximal operator');
            % function [x] = complex_mprecon(y, F, S, C, Pc, x, niter, dohogwild)
            
            tic
            % magnitude and phase images.
            lambda_range = [1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0];
            factors = [1/8 , 1/5 , 1/2 , 1 , 2 , 5 , 8];

            Lr = length(lambda_range);
            Lf = length(factors);

            nmse = @(a,b) norm(a(:) - b(:))^2 / norm(b(:))^2 ; 

            recons_range = zeros(numel(x_sub),Lr);
            L1_range = zeros(Lr,1);
            DC_range = zeros(Lr,1);

            for inner_range = 1 : Lr
                % Yn, Convergence_rate, Level , Lambda, Final_F, Final_G
                [x_recon, convergence_rate, L, Lambdas, cost_dc, cost_l1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                                        'Lambda','multiple','Levels', 3 , ...
                                                        'Fixed Lambda', lambda_range(inner_range));
                recons_range(:,inner_range) = x_recon(:);
                L1_range(inner_range) = cost_l1;
                DC_range(inner_range) = cost_dc;

            end
            %
            distance_range = sqrt(L1_range/max(L1_range).^2 + DC_range/max(DC_range).^2);
            disp(distance_range);
            [val, pos] = min(distance_range);

            recons_factors = zeros(numel(x_sub),Lf);
            L1_factors = zeros(Lf,1);
            DC_factors = zeros(Lf,1);

            for inner_factor = 1 : Lf
                % Yn, Convergence_rate, Level , Lambda, Final_F, Final_G
                [x_recon, convergence_rate, L, Lambdas, cost_dc, cost_l1] = complex_mFISTA2(y,F,S,C,x_sub,wname,...
                                                        'Lambda','multiple','Levels', 3 , ...
                                                        'Fixed Lambda', lambda_range(pos)*factors(inner_factor));
                recons_factors(:,inner_factor) = x_recon(:);
                L1_factors(inner_factor) = cost_l1;
                DC_factors(inner_factor) = cost_dc;
            end
            distance_factors = sqrt(L1_factors/max(L1_factors).^2 + DC_factors/max(DC_factors).^2);

            disp(distance_factors);
            [val, pos2] = min(distance_factors);
            recon_Lcurve = reshape(recons_factors(:,pos2),[size(x)]);
            lambda_Lcurve = lambda_range(pos)*factors(pos2);
            
             gt = C* (S' * (Ffull'*ground_truth));

            % Please create the folder :) 
            cd ./results_Lcurve
                save(strcat('Lcurve_',wname,'_recon_noisy_phantom_noiselevel_',num2str(i),...
                                            'USF:',num2str(k),...
                                           '_experiment',num2str(j),'.mat'),'recon_Lcurve','gt','x','weights','convergence_rate','number_experiment',...
                                                                            'L','lambda_Lcurve');

            cd ..


        end
    end
end
cd ..
