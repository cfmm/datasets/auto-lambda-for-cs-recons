clear all; close all; clc;

target = 'slice107';
load(strcat(target,'.mat'));

ksp_phantom = ksp/max(max(max(abs(ifft2c(ksp)))));
[X,Y,C]= size(ksp_phantom);

%% add noise to verify what would be the SNR based on SoS

vec = @(a_) a_(:);
add_noise = @( N_ , mu_ , sigma_) sigma_ * (rand(N_ , 1)-0.5) + mu_ ;
ifftnc = @(ksp) fftshift(ifftn(ifftshift(ksp))); 
fftnc = @(im) fftshift(fftn(ifftshift(im)));

noisy_phantom = zeros(size(ksp_phantom));
phantom = zeros(size(ksp_phantom));
mu_ = 0;
sigma_ = 0.00000473;

%%%% SNR   sigma_
%%%%   1.5 0.0001
%%%%   5   0.00003026
%%%%   10  0.000015
%%%%   20  0.0000074;
%%%%   30  0.00000473;

N = prod([X,Y]);
figure, hold on
for c_ = 1 : C
    %sigma_ = perc * min_re_im( complex_data_phantom(:,:,:,c_) );
    phantom(:,:,c_) = ifftnc(ksp(:,:,c_));
    noisy_phantom(:,:,c_) = ifftnc(ksp(:,:,c_) + ...
                              [reshape( add_noise( N , mu_ , sigma_ ),[X,Y]) + ...
                               1i * reshape( add_noise( N , mu_ , sigma_ ),[X,Y])]);
   imshow(abs(noisy_phantom(:,:,c_)),[])
end

sos_noisy_phantom = sqrt(sum(conj(noisy_phantom).*noisy_phantom,3));
sos_phantom = sqrt(sum(conj(phantom).*phantom,3));
imshow(abs(sos_noisy_phantom).*mask2D,[],'init',800);
mean_signal_phantom = mean(vec(sos_phantom(mask2D)));
std_bkg = std(vec(noisy_phantom(1:30,1:30))); % by inspection topleft area
SNR = mean_signal_phantom / std_bkg
%% Automatic procedure to do them all

%%%% SNR   sigma_
%%%%   1.5 0.0001
%%%%   5   0.00003026
%%%%   10  0.000015
%%%%   20  0.0000074;
%%%%   30  0.00000473;

number_of_experiments = 10;
noise_levels = [0.00000473, 0.0000074, 0.000015, 0.00003026];
snr_levels =   [ 30,  20,  10,   5]; % theoretical one

mu_ = 0;
N = prod([X,Y]);
meaning_data = {'X','Y','Coil'};
ground_truth = ksp;

mkdir('Simulations');
cd Simulations
cc = 1;
for nit = noise_levels
    disp(strcat('noise_level: ',num2str(nit)));
    for expit = 1:number_of_experiments
        
        sigma_ = nit;
        noisy_phantom = zeros(size(ksp_phantom));
        noisy_ksp = zeros(size(ksp_phantom));
        
        for c_ = 1 : C
            %sigma_ = perc * min_re_im( complex_data_phantom(:,:,:,c_) );
            noisy_ksp(:,:,c_) =    ksp(:,:,c_) + ...
                                   [reshape( add_noise( N , mu_ , sigma_ ),[X,Y]) + ...
                                   1i * reshape( add_noise( N , mu_ , sigma_ ),[X,Y])];
            noisy_phantom(:,:,c_) = ifftnc(noisy_ksp(:,:,c_));
        end
        sos_noisy_phantom = sqrt(sum(conj(noisy_phantom).*noisy_phantom,4));
        std_bkg = std(vec(noisy_phantom(1:30,1:30))); % by inspection topleft area
        SNR = mean_signal_phantom / std_bkg
        
        save(strcat('noisy_phantom_SNR_',num2str(snr_levels(cc)),'_experiment',num2str(expit),'.mat'),'meaning_data','noisy_ksp','ground_truth','mask2D','-v7.3');
        disp(strcat('noisy_phantom_SNR_',num2str(snr_levels(cc)),'_experiment',num2str(expit),'.mat stored'));
        
    end
    cc = cc + 1;
end

cd ..
%% if you want to check results

clear all; close all; clc;
%
cd Simulations
%
load noisy_phantom_SNR_10_experiment4.mat
c_ = 1;
figure, 
subplot 221, imshow(imrotate(abs(squeeze(noisy_phantom(:,107,:,c_,1))),90),[]); colorbar;
title('Magnitude');
subplot 222, imshow(imrotate(angle(squeeze(noisy_phantom(:,107,:,c_,1))),90),[]); colorbar;
title('Phase');
subplot 223, imshow(imrotate(squeeze(real(noisy_phantom(:,107,:,c_,1))),90),[]); colorbar;
title('Real');
subplot 224, imshow(imrotate(squeeze(imag(noisy_phantom(:,107,:,c_,1))),90),[]); colorbar;
title('Imaginary');
%
cd ..
%

