
clear all; close all; clc;

cd Simulations/results_Lcurve

load Lcurve_db4_recon_noisy_phantom_noiselevel_5USF:3_experiment1.mat;
load('noisy_phantom_SNR_5_experiment1','mask2D');
mask = mask2D; 

size_xyz = size(gt);
N = 10;
wname = 'db4'; % symlet , dmeyer , db4
noise_level = [5,10,20,30];
USF = [3:10];
NU = length(USF);
NN = length(noise_level);

map_weights = zeros([prod(size_xyz), NN, NU]);
map_x_weights = zeros([prod(size_xyz), NN, NU]);
map_x_recon_weights = zeros([prod(size_xyz), NN, NU]);

nmse = @(a_ , b_) norm(a_(:)-b_(:))^2 / norm(b_(:))^2;
num_images = 2; % mag, phi

m_nmse_mat = zeros(N, NN, NU);
m_pc_mat = zeros(N, NN, NU);
m_hfen_mat = zeros(N, NN, NU);
m_ssim_mat = zeros(N, NN, NU);

phi_nmse_mat = zeros(N, NN, NU);
phi_pc_mat = zeros(N, NN, NU);
phi_hfen_mat = zeros(N, NN, NU);
phi_ssim_mat = zeros(N, NN, NU);

re_nmse_mat = zeros(N, NN, NU);
re_pc_mat = zeros(N, NN, NU);
re_hfen_mat = zeros(N, NN, NU);
re_ssim_mat = zeros(N, NN, NU);

im_nmse_mat = zeros(N, NN, NU);
im_pc_mat = zeros(N, NN, NU);
im_hfen_mat = zeros(N, NN, NU);
im_ssim_mat = zeros(N, NN, NU);


L_ =  zeros(N, NU, NN);
%
for j = 1 : NN 
    disp(strcat('running analysis on noise level:',num2str(noise_level(j))));
    
    for k = 1 : NU
        disp(strcat('USF:',num2str(USF(k))));
        for i = 1 : N
            disp(strcat('exp:',num2str(i)));

            load(strcat('Lcurve_',wname, '_recon_noisy_phantom_noiselevel_',num2str(noise_level(j)),...
                'USF:',num2str(USF(k)),'_experiment',num2str(i),'.mat'));

            disp('computing stuff...');

                L_(i, k, j) = L;
                x_reconi = reshape(recon_Lcurve,size(gt));
                
                map_weights(:,j,k) = weights(:);
                map_x_weights(:,j,k) = x(:);
                map_x_recon_weights(:,j,k) = x_reconi(:);

                mrec = x_reconi.*mask;
                mref = gt.*mask;
                nmse_phase = angle(mref .* conj(mrec));
                
                m_nmse_mat(i,j,k) = nmse(abs(mrec(mask)),abs(mref(mask)));
                phi_nmse_mat(i,j,k) = norm(nmse_phase(mask))^2 / norm(vec(angle(mref(mask))))^2;
                re_nmse_mat(i,j,k) = nmse(real(mrec(mask)),real(mref(mask)));
                im_nmse_mat(i,j,k) = nmse(imag(mrec(mask)),imag(mref(mask)));
                
                m_hfen_mat(i,j,k) = compute_hfen(abs(mrec),abs(mref));
                phi_hfen_mat(i,j,k) = compute_hfen(angle(mrec),angle(mref));
                re_hfen_mat(i,j,k) = compute_hfen(real(mrec),real(mref));
                im_hfen_mat(i,j,k) = compute_hfen(imag(mrec),imag(mref));
                
                m_pc_mat(i,j,k) = 1-pcCoeff(abs(mrec(mask)),abs(mref(mask)));
                phi_pc_mat(i,j,k) = 1-pcCoeff(angle(mrec(mask)),angle(mref(mask)));
                re_pc_mat(i,j,k) = 1-pcCoeff(real(mrec(mask)),real(mref(mask)));
                im_pc_mat(i,j,k) = 1-pcCoeff(imag(mrec(mask)),imag(mref(mask)));
                
                m_ssim_mat(i,j,k) = 1-compute_xsim(abs(mrec),abs(mref));
                phi_ssim_mat(i,j,k) = 1-compute_xsim(angle(mrec),angle(mref));
                re_ssim_mat(i,j,k) = 1-compute_xsim(real(mrec),real(mref));
                im_ssim_mat(i,j,k) = 1-compute_xsim(imag(mrec),imag(mref));
        end
        
    end
end


save('results_LCURVE',...
    'm_nmse_mat','m_hfen_mat','m_pc_mat','m_ssim_mat',...
  'phi_nmse_mat','phi_hfen_mat','phi_pc_mat','phi_ssim_mat',...
  're_nmse_mat','re_hfen_mat','re_pc_mat','re_ssim_mat',...
  'im_nmse_mat','im_hfen_mat','im_pc_mat','im_ssim_mat');

cd ..
cd ..
